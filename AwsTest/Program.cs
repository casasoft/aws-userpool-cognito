﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;
using Amazon.CognitoIdentityProvider;
using Amazon.CognitoIdentityProvider.Model;
using Amazon.Extensions.CognitoAuthentication;
using Amazon.Runtime;

namespace AwsTest
{
    class Program
    {
        private const string PoolID = "us-east-2_NC7PyMDMF";
        private const string AppClientId = "1u40mobfp1am9pmmagi99fo121";
        private static Amazon.RegionEndpoint Region = Amazon.RegionEndpoint.USEast2;
        static async Task Main(string[] args)
        {

            //await SignUp();
            await SignIn();
            Console.Read();
        }

        static async Task SignUp()
        {
            Console.Write("Username:");
            string username = Console.ReadLine();
            Console.Write("Password:");
            string password = Console.ReadLine();
            Console.Write("Email:");
            string email = Console.ReadLine();

            var provider = new AmazonCognitoIdentityProviderClient(new AnonymousAWSCredentials(), Region);

            var signUpRequest = new SignUpRequest
            {
                ClientId = AppClientId,
                Username = username,
                Password = password
            };

            var attributes = new List<AttributeType>
            {
                new AttributeType {Name = "email", Value = email}
            };
            signUpRequest.UserAttributes = attributes;
            try
            {
                var result = await provider.SignUpAsync(signUpRequest);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }

            Console.Write("User Created Successfully!");
        }

        static async Task SignIn()
        {
            Console.Write("Username:");
            string username = Console.ReadLine();
            Console.Write("Password:");
            string password = Console.ReadLine();

            var provider = new AmazonCognitoIdentityProviderClient(new AnonymousAWSCredentials(), Region);

            var userPool = new CognitoUserPool(PoolID, AppClientId, provider);
            var user = new CognitoUser(username, AppClientId, userPool, provider);

            var authRequest = new InitiateSrpAuthRequest()
            {
                Password = password
            };

            AuthFlowResponse authResponse = null;
            try
            {
                authResponse = await user.StartWithSrpAuthAsync(authRequest).ConfigureAwait(false);

                var getUserRequest = new GetUserRequest();
                getUserRequest.AccessToken = authResponse.AuthenticationResult.AccessToken;

                var getUser = await provider.GetUserAsync(getUserRequest);
                if (getUser.HttpStatusCode == HttpStatusCode.OK)
                {
                    Console.Write("Login Successful");
                }

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                
            }

          
        }
    }
}
